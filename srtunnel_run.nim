import std/[os, strutils]
import strformat
import osproc
import json
import argparse


proc gen_key(ekey: string, comment: string, user:string, debug:bool): int =
  if fileExists(ekey):
    if debug == true:
      stderr.writeLine(fmt"info: {ekey} in place")
    return 0
  else:
    var cmd = fmt"chpst -u {user}:users ssh-keygen -t ed25519 "
    cmd = cmd & fmt"-f {ekey} -q -N '' -C {comment}"
    let errC = execCmd(cmd)
    if errC > 0:
      stderr.writeLine(fmt"warning: {cmd} failed")
    return errC


proc get_first_nic_mac(): string =
  var nicpath = "/sys/class/net/eth0/address"
  for kind, path in walkDir("/sys/class/net"):
    if path.endsWith("lo"):
      continue
    nicpath = path & "/address"
    break
  return readFile(nicpath).strip()


proc get_lid(sconfdir: string, hkey: string): string =
  try:
    return readFile(fmt"{sconfdir}/lid").strip()
  except:
    var
      cmd = fmt"sha1sum {hkey}"
      o = execProcess(cmd).split()
    try:
      writeFile(fmt"{sconfdir}/lid", fmt("{o[0]}\n"))
    except:
      stderr.writeLine("warning: can't save lid")
    return o[0].strip()


proc get_config(gateway: string, ekey: string, dkey: string, user: string,
                sconfdir: string, debug:bool, port=443): string =
  let pub = readFile(fmt"{ekey}.pub")
  var cmd = fmt"chpst -u {user} ssh -o StrictHostKeyChecking=accept-new "
  cmd = cmd & fmt"-o ConnectTimeout=45 -o ServerAliveInterval=10 -p {port} "
  cmd = cmd & fmt"-i {dkey} gwuser@{gateway} {pub}"
  var config = execProcess(cmd)
  if debug == true:
    stderr.writeLine(fmt"debug: {cmd}")
    stderr.writeLine(fmt"debug: {config}")
  try:
    writeFile(fmt"{sconfdir}/srtunnel.config", config)
  except:
    stderr.writeLine("warning: can't save config")
  return config


proc serve(rssh:int, ekey: string, debug:bool, rhttp=0, gateway="192.168.5.90", port=443): int =
  var rrhttp: int
  if rhttp == 0:
    rrhttp = rssh - 1
  else:
    rrhttp = rhttp
  var cmd = "ssh -q -o StrictHostKeyChecking=accept-new "
  cmd = cmd & "-o ConnectTimeout=45 "
  cmd = cmd & "-o ServerAliveInterval=30 "
  cmd = cmd & fmt"-p {port} -N "
  cmd = cmd & fmt"-i {ekey} "
  cmd = cmd & fmt"-R {rssh}:localhost:22 -R {rrhttp}:localhost:443 "
  cmd = cmd & fmt"gwdata@{gateway}"
  if debug == true:
    stderr.writeLine(fmt"debug: {cmd}")
  let errC = execCmd(cmd)
  return errC


when isMainModule:
  var
    user = "datalog"
    sconfdir = "/etc/srtunnel"
    hkey = "/etc/ssh/ssh_host_rsa_key.pub"
    homedir = fmt"/home/{user}"
    ekey = fmt"{homedir}/.ssh/id_ed25519_gw"
    config = fmt"{sconfdir}/srtunnel.config"
    dkey = fmt"{sconfdir}/id_rsa_default"
    debug: bool
    wait: bool

  var p = newParser:
    option("-c", "--config")
    flag("-d", "--debug")
    flag("-w", "--wait")

  try:
    var opts = p.parse()
    debug = opts.debug
    wait = opts.wait
    let appconfig = readFile(opts.config).splitlines()
    for ac in appconfig:
      if ac.startsWith("user"):
        user = ac.split("=")[1]
      if ac.startsWith("hkey"):
        hkey = ac.split("=")[1]
      if ac.startsWith("sconfdir"):
        sconfdir = ac.split("=")[1]
      if ac.startsWith("dkey"):
        dkey = ac.split("=")[1]

  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo(err.help)
      quit(1)
  except UsageError:
    stderr.writeLine(getCurrentExceptionMsg())
    quit(1)
  except IOError:
    if debug == true:
      stderr.writeLine("info: can't read app.config, use defaults")
      stderr.writeLine(fmt"#sconfdir={sconfdir}")
      stderr.writeLine(fmt"#user={user}")
      stderr.writeLine(fmt"#hkey={hkey}")
      stderr.writeLine(fmt"#dkey={dkey}")

  homedir = fmt"/home/{user}"
  ekey = fmt"{homedir}/.ssh/id_ed25519_gw"
  config = fmt"{sconfdir}/srtunnel.config"
  if dirExists(homedir) == false:
    stderr.writeLine(fmt"error: can't found {homedir}, setup the {user} first")
    quit(2)

  var
    comment = get_lid(sconfdir, hkey) & get_first_nic_mac()
    cconfig: string
    gateway, gateway1, gateway2: string
    gw_file = "/tmp/next_gateway"
    rssh: int
    rhttp: int
  discard gen_key(ekey, comment, user, debug)
  try:
    var conf = readFile("./conf").splitlines()
    for line in conf:
      if line.startsWith("GATEWAY1"):
        let ngw = line.split("=")
        gateway1 = ngw[1]
      if line.startsWith("GATEWAY2"):
        let ngw = line.split("=")
        gateway2 = ngw[1]
      if line.startsWith("GW_FILE"):
        let ngw = line.split("=")
        gw_file = ngw[1].replace("\"", "")
    try:
      gateway = readFile(gw_file).strip()
    except:
      gateway = gateway1
  except:
    stderr.writeLine(fmt"error: can't load gateway config from: ./conf")
    quit(3)

  try:
    cconfig = readFile(config)
    var _ = cconfig[1]
  except:
    cconfig = get_config(gateway, ekey, dkey, user, sconfdir, debug)
    if getFilesize(config) == 0:
      try:
        removeFile(config)
      except:
        stderr.writeLine(fmt"error: can't delete not valid config {config}")
      if wait == true:
        echo(fmt"exit with: 4, wait 10 seconds")
        sleep(10_000)
      quit(4)

  try:
    var
      cdata = parseJson(cconfig)
      cname = cdata["name"].getStr()
    if cname != get_lid(sconfdir, hkey):
      stderr.writeLine(fmt"error: client config name not valid")
      quit(5)

    try:
      rssh = cdata["rports"]["RSSH"].getInt()
    except KeyError:
      stderr.writeLine(fmt"error: client config not valid")
      quit(6)
    try:
      rhttp = cdata["rports"]["RHTTP"].getInt()
    except:
      stderr.writeLine(fmt"warning: client use default rhttp")
  except:
      stderr.writeLine(fmt"error: can't load client config")
      quit(7)

  if rssh == 0:
    let cerror = readFile(config)
    removeFile(config)
    stderr.writeLine(fmt"error: can't get config: {cerror}")
    quit(5)

  echo(fmt"serve {rssh} to: {gateway}")
  let errC = serve(rssh, ekey, debug, rhttp, gateway)
  try:
    if gateway == gateway1:
      writeFile(gw_file, fmt("{gateway2}\n"))
    else:
      writeFile(gw_file, fmt("{gateway1}\n"))
  except:
    stderr.writeLine(fmt"can't save next_gateway to: {gw_file}")

  if debug == true:
    stderr.writeLine(fmt"debug: exit with {errC}")
  if wait == true:
    echo(fmt"exit with: {errC}, wait 10 seconds")
    sleep(10_000)
  quit(errC)

  # nim c -d:release --opt:size --passL:-static srtunnel_run.nim
  #
  # app.config
  #
  #user=datalog
  #sconfdir=/etc/srtunnel
  #hkey=/etc/ssh/ssh_host_rsa_key.pub
  #dkey=/etc/srtunnel/id_rsa_default

  # example ./conf
  # GATEWAY1=192.168.55.31
  # GATEWAY2=192.168.66.32
  # GW_FILE="/tmp/next_gateway"
